const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

io.on('connection', (socket) => {
    console.log('Mis utilisateur connecté');

    socket.on("get_drawing", (data) => {
        io.emit("draw", data);
    })
});

http.listen(3000, () => {
    console.log('listening on 3000');
});